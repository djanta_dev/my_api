import React, { useContext } from "react";
import {NavLink} from "react-router-dom";
import AuthAPI from "../services/authAPI";
import AuthContext from "../contexts/AuthContext";
import {toast} from "react-toastify";

const Navbar = ({ history }) => {

    const {isAuthenticated, setIsAuthenticated} = useContext(AuthContext);

    const handleLogout = () => {
        AuthAPI.logout();
        toast.info("VOus êtes désormais déconnecté ! ");
        setIsAuthenticated(false);
        history.push("/login");
    };

    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="container-fluid">
            <NavLink className="navbar-brand" to="/">DemoApp!</NavLink>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor02"
                    aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarColor02">
                <ul className="navbar-nav me-auto">

                    <li className="nav-item">
                        <NavLink className="nav-link" to="/customers">Clients</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/invoices">Factures</NavLink>
                    </li>
                </ul>
                <ul className="navbar-nav ml-auto">
                    {(!isAuthenticated && (<>
                        <li className="nav-item my-2 me-sm-2">
                            <NavLink to="/register" className="btn btn-link">Inscription</NavLink>
                        </li>
                        <li className="nav-item my-2 me-sm-2">
                            <NavLink to="/login" className="btn btn-outline-success">Connexion!</NavLink>
                        </li>)
                    </>)) || (
                    <li className="nav-item my-2">
                        <button onClick={handleLogout} className="btn btn-outline-danger">Déconnexion</button>
                    </li>)}
                </ul>

            </div>
        </div>
    </nav>
    );
}
export default Navbar;
