import React from 'react';

const Field = ({ name, label, value, onChange, placeholder="", type="text", error=""  }) =>
    (
        <div className="input-group mb-3">
            <div className="input-group-append">
                <label htmlFor={ name } className="input-group-text mb-1">{ label }</label>
            </div>
            <input
                type={ type }
                name={ name }
                id= { name }
                className={"form-control input_user" + (error &&" is-invalid")}
                placeholder={ placeholder || label}
                value={ value }
                onChange={onChange}
            />
            {error && <p className="invalid-feedback">{error}</p>}
        </div>
    );


export default Field;
