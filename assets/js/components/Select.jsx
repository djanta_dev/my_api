import React from 'react';

const Select = ({ name, label, value, onChange, placeholder="", type="text", error="", children  }) => {
    return (
        <div className="input-group mb-3">
            <div className="input-group-append">
                <label htmlFor={ name } className="input-group-text mb-1">{ label }</label>
            </div>
            <select onChange={onChange} name={name} id={name} value={value} className={"form-control" + (error &&" is-invalid")}>
                { children }
            </select>
            {error && <p className="invalid-feedback">{error}</p>}
        </div>
    );
};

export default Select;
