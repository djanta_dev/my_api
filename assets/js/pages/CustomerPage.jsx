import React, { useState, useEffect } from 'react';
import Field from "../components/Field";
import { Link } from "react-router-dom";
import CustomerAPI from  "../services/customerAPI";
import { toast } from "react-toastify";
import FormContentLoader from "../components/loaders/FormContentLoader";

const CustomerPage = ({ match, history }) => {

    const { id = "new" } = match.params ;
    const [customer, setCustomer] = useState({
        lastName: "",
        firstName: "",
        email: "",
        company: ""
    });

    const [errors, setErrors] = useState({
        lastName: "",
        firstName: "",
        email: "",
        company: ""
    });
    const [loading, setLoading] = useState(false);
    const [editing, setEditing] = useState(false);

    const fetchCustomer =  async id =>  {
        try{
            const data = await CustomerAPI.find(id);
            const { lastName, firstName, email, company } = data;
            setCustomer({ lastName, firstName, email, company } );
            setLoading(false);
        }catch (error) {
            toast.error("Le client n'a pas pu être chargé");
            history.replace("/customers")
        }

    };

    //Chargement du customer si besoin au changement de l'idenfiant
    useEffect(() => {
        if(id !== "new") {
            setEditing(  true);
            setLoading(true);
            fetchCustomer(id);
        }
    }, [id]);


    // Gestion des changement des input dans le formulmaire
    const handleChange = ({currentTarget}) => {
        const {value, name} = currentTarget;
        setCustomer({...customer, [name]: value});
    };


    const handleSubmit = async event => {
        event.preventDefault();

        //http://localhost:8000/api/customers
        try{
            setErrors({});
            if(editing){
                await CustomerAPI.update(id, customer);
                toast.success("Le client a bien été modifié");
            }else{
                await CustomerAPI.create(customer);
                toast.success("Le client a bien été créé");
                history.replace("/customers");
            }
        }catch ({ response }) {
            const { violations } = response.data;
            if(violations){
                const apiErrors = {};
                violations.forEach(({ propertyPath, message}) => {
                    apiErrors[propertyPath]= message;
                });
                setErrors(apiErrors);
                toast.error("Des erreurs dans votre formulaire !");
            }
        }
        //console.log(customer);
    };
    
    return (
        <>

            {(!editing && <h1>Création d'un client</h1>) || (<h1>Modification du client</h1>) }
            {loading && <FormContentLoader/>}
            {!loading && (<form onSubmit={handleSubmit}>
                <Field 
                    name="lastName" 
                    label="Nom de famille" 
                    placeholder="Saisir le nom de famille du client"
                    value= {customer.lastName}
                    onChange={handleChange}
                    error={errors.lastName}
                />
                <Field 
                    name="firstName" 
                    label="Prénom" 
                    placeholder="Saisir le prénom de famille client"
                    value={customer.firstName}
                    onChange={handleChange}
                    error={errors.firstName}
                />
                <Field 
                    name="email" 
                    label="Email" 
                    placeholder="Saisir l'email du client" 
                    type="email"
                    value={customer.email}
                    onChange={handleChange}
                    error={errors.email}
                />
                <Field 
                    name="company" 
                    label="Entreprise"
                    placeholder="Saisir l'entreprise du client" 
                    value={customer.company}
                    onChange={handleChange}
                    error={errors.company}
                />
                <div className="form-group d-flex justify-content-center mt-3">
                    <button className="btn btn-success rounded ml-1">Enregistrer</button>
                    <Link to="/customers" className="btn btn-link" >Retour à la liste des clients</Link>
                </div>
            </form>)}
        </>
    );
};


export default CustomerPage;
