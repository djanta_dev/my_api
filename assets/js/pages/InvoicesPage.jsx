
import  React, { useEffect, useState } from "react";
import InvoiceAPI from "../services/invoiceAPI";
import Pagination from "../components/Pagination";
import moment from "moment";
import {Link} from "react-router-dom";
import {toast} from "react-toastify";
import TableLoader from "../components/loaders/TableLoader";

const STATUS_CLASSES = {
    PAID : "primary",
    SENT : "info",
    CANCELLED : "danger"
};

const STATUS_LABELS = {
    PAID : "Payée",
    SENT : "Envoyée",
    CANCELLED : "Annulée"
};

const InvoicesPage = props => {
    // Création d'une constante a base de useSate qui va retourner un tableau des clients
    const  [invoices, setInvoices] = useState([]);
    // Création d'une constante permettant de gérer la page courante pour la pagination
    const [currentPage, setCurrentPage] = useState(1);
    const [search, setSearch] = useState('');
    const  [loading, setLoading] = useState(true);

    const fetchInvoices = async () => {
        try{
            const data = await InvoiceAPI.findAll();
            setInvoices(data);
            setLoading(false);
        }catch (error) {
            toast.error("Erreur lors du chargement des factures !");
        }
    };
    //Récupération des données de la base de donnée avec axios à partir d'un useEffect et remplis la variable des customers
    useEffect(()=>{
        fetchInvoices();
    }, []);

    // Fonction de supression d'un clients au cas ou celui n'a pas de factures
    const handleDelete = async (id) => {
        //Copie de la liste original des clients
        const originalInvoices = [...invoices];
        setInvoices( invoices.filter(invoice => invoice.id !== id ));

        try{
            await InvoiceAPI.delete(id);
            toast.success("La facture a bien été supprimé");
        }catch (error) {
            toast.error("Une erreur est survenue");
            setInvoices(originalInvoices);
        }
    };

    const formatDate = (str) => moment(str).format('DD/MM/YYYY');

    const handlePageChange = (page) =>{
        setCurrentPage(page);
    };

    const handleSearch = ({currentTarget}) => {
        setSearch(currentTarget.value);
        setCurrentPage(1);
    };

    const itemsPerPage = 10 ;
    //Filtrage des invoices suivant la recherche
    const filteredInvoices = invoices.filter(
        i =>
            i.customer.firstName.toLowerCase().includes(search.toLowerCase()) ||
            i.customer.lastName.toLowerCase().includes(search.toLowerCase()) ||
            i.amount.toString().toLowerCase().startsWith(search.toLowerCase()) ||
            STATUS_LABELS[i.status].toLowerCase().includes(search.toLowerCase())
    );

    const paginatedInvoices = Pagination.getData(
        filteredInvoices,
        currentPage,
        itemsPerPage
    );

    return (
        <>
            <div className="d-flex justify-content-between align-items-center">
                <h1> Liste des Factures  </h1>
                <Link to="/invoices/new" className="btn btn-outline-success rounded">Créer une facture</Link>
            </div>
            <div className="form-group">
                <input type="text" onChange={handleSearch} value={search} className="form-control" placeholder="Rechercher ..."/>
            </div>
            <table className="table table-hover">
                <thead>
                    <tr>
                        <th>Numéro</th>
                        <th>Client</th>
                        <th className="text-center">Date d'envoi</th>
                        <th className="text-center">Statut</th>
                        <th className="text-center">Montant</th>
                        <th className="text-right"> </th>
                    </tr>
                </thead>
                {!loading && <tbody>
                    {paginatedInvoices.map(invoice =>
                        <tr key={invoice.id}>
                            <td className="text-center">{ invoice.chrono }</td>
                            <td>
                                <Link to={`/invoices/${invoice.id}`}>{ invoice.customer.firstName } { invoice.customer.lastName }</Link>
                            </td>
                            <td className="text-center">{ formatDate(invoice.sentAt) }</td>
                            <td className="text-center">
                                    <span className={"badge bg-" + STATUS_CLASSES[invoice.status]}>{STATUS_LABELS[invoice.status]}</span>
                            </td>
                            <td className="text-right">{invoice.amount.toLocaleString()} $</td>
                            <td>
                                <Link to={`/invoices/${invoice.id}`}><div className="btn btn-sm btn-primary mr0-l"><i className="fas fa-edit"></i></div></Link>
                                <button
                                    onClick={() => handleDelete(invoice.id)}
                                    className="btn btn-sm btn-danger"><i className="fas fa-trash"></i></button>
                            </td>
                        </tr>
                    )}
                </tbody>}
            </table>
            {loading && <TableLoader/>}

            {itemsPerPage < filteredInvoices.length && <Pagination
                currentPage={currentPage}
                itemsPerPage={itemsPerPage}
                length={filteredInvoices.length}
                onPageChanged={handlePageChange} />}

        </>
    );
};

export  default InvoicesPage;
