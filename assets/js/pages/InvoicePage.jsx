import React,{ useState, useEffect} from 'react';
import InvoiceAPI from "../services/invoiceAPI";
import Field from "../components/Field";
import { Link } from "react-router-dom";
import Select from "../components/Select";
import CustomersAPI from "../services/customerAPI";
import { toast } from "react-toastify";
import FormContentLoader from "../components/loaders/FormContentLoader";


const InvoicePage = ({match, history}) => {

    const { id = "new" } = match.params ;
    const [invoice, setInvoice] = useState({
        "amount" : "",
        "status" : "SENT",
        "customer": ""
    });

    const [customers, setCustomers] = useState([]);
    const [editing, setEditing] = useState(false);
    const [errors, setErrors] = useState({
        "customer" : "",
        "amount" : "",
        "status" : ""
    });
    const [loading, setLoading] = useState(true);

    const fetchCustomers = async  () => {
        try {
            const data =  await CustomersAPI.findAll();
            setCustomers(data);
            setLoading(false);
            if(!invoice.customer) setInvoice({...invoice, customer: data[0].id});
        }catch (error) {
            toast.error("Impossible de charger les clients");
            history.replace('/invoices');
        }
    };

    const fetchInvoice = async  id => {
        try {
            const {amount, status, customer}  = await  InvoiceAPI.find(id);
            setInvoice({amount, status, customer: customer.id });
            setLoading(false);
        }catch (error) {
            toast.error("Impossible de charger la facture demandée");
            history.replace('/invoices');
        }
    };

    useEffect(() => {
        fetchCustomers();
    }, []);

    useEffect(()=> {
        if(id !== "new"){
            setEditing(true);
            fetchInvoice(id);
        }
    }, [id]);

    const handleChange = ({currentTarget}) => {
        const {value, name} = currentTarget;
        setInvoice({...invoice, [name]: value});
    };

    const  handleSubmit = async (event) => {
        event.preventDefault();
        console.log(editing);
        try{
            setErrors({});
            if(editing){
                await InvoiceAPI.update(id, invoice);
                toast.success("La facture a bien été modifiée");
            }else{
                await InvoiceAPI.create(id, invoice);
                toast.success("La facture a bien été enregistrée");
                history.replace("/invoices");
            }
        }catch (error) {
            const { violations } = response.data;
            if(violations){
                const apiErrors = {};
                violations.forEach(({ propertyPath, message}) => {
                    apiErrors[propertyPath]= message;
                });
                setErrors(apiErrors);
                toast.error("Des erreurs dans votre formulaire");
            }
        }

    };


    return (
        <>
            {editing && <h1>Modification d'une facture</h1> || <h1>Création d'une facture</h1>}
            {loading && <FormContentLoader />}
            {!loading && (<form onSubmit={handleSubmit} >
                <Field name="amount" type="number" placeholder="Montant de la facture" label="Montant"  onChange={ handleChange } value={ invoice.amount } error={errors.amount} />
                <Select name="customer" label="Client" value = {invoice .customer} onChange = {handleChange} >
                    { customers.map(customer  => (
                        <option key={customer.id} value={customer.id}>{customer.firstName} {customer.lastName}</option>
                    ))}
                </Select>
                <Select name="status" label="Client" value = {invoice.customer} onChange = {handleChange} >
                    <option value="SENT">Envoyée</option>
                    <option value="PAID">Payée</option>
                    <option value="CANCELLED">Annulée</option>
                </Select>
                <div className="form-group d-flex justify-content-center mt-3">
                    <button className="btn btn-success rounded ml-1">Enregistrer</button>
                    <Link to="/invoices" className="btn btn-link" >Retour à la liste des factures</Link>
                </div>


            </form>)}
        </>
    );
};

export default InvoicePage;
