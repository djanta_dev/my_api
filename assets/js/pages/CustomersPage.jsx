
import  React, { useEffect, useState } from "react";
import axios from "axios";
import CustomerAPI from "../services/customerAPI";
import Pagination from "../components/Pagination";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import TableLoader from "../components/loaders/TableLoader";

const CustomersPage = props => {
    // Création d'une constante a base de useSate qui va retourner un tableau des clients
    const  [customers, setCustomers] = useState([]);
    // Création d'une constante permettant de gérer la page courante pour la pagination
    const [currentPage, setCurrentPage] = useState(1);
    const [search, setSearch] = useState('');
    const [loading, setLoading] = useState(true);

    const fetchCustomers = async () => {
        try{
            const data = await CustomerAPI.findAll();
            setCustomers(data);
            setLoading(false);
        }catch (error) {
            toast.error("Impossible de charger les clients");
        }
    };
    //Récupération des données de la base de donnée avec axios à partir d'un useEffect et remplis la variable des customers
    useEffect(()=>{
        fetchCustomers();
    }, []);

    // Fonction de supression d'un clients au cas ou celui n'a pas de factures
    const handleDelete = async (id) => {
        //Copie de la liste original des clients
        const originalCustomers = [...customers];

        //console.log(id);
        // Approche optimiste on elimine en immédiatement la donnée en cas d'erreur on la retourne
        customers.filter(customer => customer.id !== id );

        // Ceci est une approche pessimiste qui permet de retirer le clients de la liste une fois qu'elle a bien été supprimer de la base de donnée par l'API en cas d'erreur rien n'est fait.
        try{
            await CustomerAPI.delete(id);
            toast.success("Le client a bien été supprimé");
        }catch (error) {
            setCustomers(originalCustomers);
            toast.error("La suppression du client n'a pas pu fonctionner");
        }
    };

    const handlePageChange = (page) =>{
        setCurrentPage(page);
    };

    const handleSearch = ({currentTarget}) => {
        setSearch(currentTarget.value);
        setCurrentPage(1);
    };

    const itemsPerPage = 10 ;
    //Filtrage des customers suivant la recherche
    const filteredCustomers = customers.filter(
        c =>
            c.firstName.toLowerCase().includes(search.toLowerCase()) ||
            c.lastName.toLowerCase().includes(search.toLowerCase()) ||
            (c.company && c.company.toLowerCase().includes(search.toLowerCase())) ||
            c.email.toLowerCase().includes(search.toLowerCase())
    );

    const paginatedCustomers = Pagination.getData(
        filteredCustomers,
        currentPage,
        itemsPerPage
    );

    return (
        <>
            <div className="d-flex justify-content-between align-items-center">
                <h1> Liste des Clients </h1>
                <Link to="/customers/new" className="btn btn-outline-success rounded">Créer un client</Link>
            </div>

            <div className="form-group">
                <input type="text" onChange={handleSearch} value={search} className="form-control" placeholder="Rechercher ..."/>
            </div>
            <table className="table table-hover">
                <thead>
                    <tr>
                        <th className="text-center">id</th>
                        <th>Clients</th>
                        <th>Email</th>
                        <th>Entreprise</th>
                        <th className="text-center">Factures</th>
                        <th>Montant Total</th>
                        <th>Montant impayés</th>
                        <th> </th>
                    </tr>
                </thead>
                {!loading && <tbody>
                    {paginatedCustomers.map(customer =>
                        <tr key={customer.id}>
                        <td className="text-center">{ customer.id }</td>
                        <td>
                            <Link to={`/customers/${customer.id}`}>{ customer.firstName } { customer.lastName }</Link>
                        </td>
                        <td>{ customer.email }</td>
                        <td>{ customer.company }</td>
                        <td className="text-center">
                            <span className="badge bg-info">{ customer.invoices.length }</span>
                        </td>
                        <td className="text-right">{ customer.totalAmount.toLocaleString() } $</td>
                        <td className="text-right">{ customer.unpaidAmount.toLocaleString() } $</td>
                        <td>
                            <Link to={`/customers/${customer.id}`}><div className="btn btn-sm btn-primary"><i className="fas fa-edit"></i></div></Link>
                            <button
                                onClick={() => handleDelete(customer.id)}
                                disabled={ customer.invoices.length > 0}
                                className="btn btn-sm btn-danger"><i className="fas fa-trash"></i></button>
                        </td>
                    </tr>
                    )}
                </tbody>}

            </table>
            {loading &&<TableLoader/>}

            {itemsPerPage < filteredCustomers.length && <Pagination
                currentPage={currentPage}
                itemsPerPage={itemsPerPage}
                length={filteredCustomers.length}
                onPageChanged={handlePageChange} />}

        </>
    );
};

export  default CustomersPage;
