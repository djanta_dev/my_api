import React, {useState, useContext} from "react";

import AuthAPI from "../services/authAPI";
import AuthContext from "../contexts/AuthContext";
import Field from "../components/Field";
import {toast} from "react-toastify";


const LoginPage = ({ history}) => {
    const { setIsAuthenticated} = useContext(AuthContext);
    const [credentials, setCredentials] = useState({
        username : "",
        password : ""
    });
    const [error, setError] = useState("");

    const handleChange = ({currentTarget}) => {
        const {value, name} = currentTarget;
        setCredentials({...credentials, [name]: value});
    };

    const handleSubmit = async event => {
        event.preventDefault();
        try{
            await  AuthAPI.authenticate(credentials);
            setError("");
            setIsAuthenticated(true);
            toast.success("Vous êtes bien connecté !");
            history.replace("/customers");
        }catch(error){
            setError("Aucun compte ne possède cette adresse email ou les informations sont invalides");
        }
        toast.error("Une erreur est survenu !");

        //console.log(credentials);
    };

    return (
        <>
            <div className="container h-100">
                <div className="d-flex justify-content-center h-100">
                    <div className="user_card">
                        <div className="d-flex justify-content-center">
                            <form onSubmit={handleSubmit} >
                                < Field
                                    name="username"
                                    label="Adresse Email"
                                    onChange={handleChange}
                                    value={credentials.username}
                                    type="email"
                                    placeholder="Adresse email de connexion"
                                    error={error}
                                />
                                < Field
                                    name="password"
                                    label="Mot de passe"
                                    onChange={handleChange}
                                    value={credentials.password}
                                    type="password"
                                    error={error}
                                />


                                <div className="d-flex justify-content-center mt-3 login_container">
                                    <button type="submit" name="button" className="btn login_btn">Se connecter</button>
                                </div>
                            </form>
                        </div>

                        <div className="mt-4">
                            <div className="d-flex justify-content-center links">
                                Vous n'avez pas de compte ? <a href="#" className="ml-2">S'inscrire</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default LoginPage;
