/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

import React, {useState, useContext} from "react";
import ReactDOM from "react-dom";
import {HashRouter, Switch, Route, withRouter} from "react-router-dom";

import Navbar from "./js/components/Navbar";
import PrivateRoute from "./js/components/PrivateRoute"
import HomePage from "./js/pages/HomePage";
import InvoicesPage from "./js/pages/InvoicesPage";
import CustomersPage from "./js/pages/CustomersPage";
import LoginPage from "./js/pages/LoginPage";
import CustomerPage from "./js/pages/CustomerPage";
import InvoicePage from "./js/pages/InvoicePage";
import RegisterPage from "./js/pages/RegisterPage";
import AuthAPI from "./js/services/authAPI";
import AuthContext from "./js/contexts/AuthContext";
import {ToastContainer , toast} from "react-toastify";

// start the Stimulus application
import './bootstrap';
// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';
import 'react-toastify/dist/ReactToastify.css';




AuthAPI.setup();



const App = () => {
    const NavbarWithRouter = withRouter(Navbar);
    const [isAuthenticated, setIsAuthenticated] = useState(AuthAPI.isAuthenticated());

    return (
        <AuthContext.Provider value = {{
            isAuthenticated,
            setIsAuthenticated
        }}>
            <HashRouter>
                <NavbarWithRouter/>
                <main className="container pt-5">
                    <Switch>
                        <Route path="/login" component ={ LoginPage } />
                        <Route path="/register" component ={ RegisterPage } />
                        <PrivateRoute path="/invoices/:id" component={InvoicePage} />
                        <PrivateRoute path="/invoices" component={InvoicesPage} />
                        <PrivateRoute path="/customers/:id" component={CustomerPage} />
                        <PrivateRoute path="/customers" component={CustomersPage} />
                        <Route path="/" component={HomePage} />
                    </Switch>
                </main>
            </HashRouter>
        <ToastContainer position={toast.POSITION.TOP_LEFT} />
        </AuthContext.Provider>
        );

};


const rootElement = document.querySelector('#app');
ReactDOM.render(<App />, rootElement);
