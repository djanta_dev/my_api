<?php


namespace App\Events;


use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * Permet d'encoder le mot de passe de l'utilisateur à la création
 * Class PasswordEncoderSubscriber
 * @package App\Events
 */
class PasswordEncoderSubscriber implements EventSubscriberInterface
{
    /**
     * @var UserPasswordHasherInterface/
     *
     */
    private $encoder;

    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public static function getSubscribedEvents()
    {
        //Permet de mettre en place l'encodeur juste avant l'écriture dans la base de donnée
       return [
            KernelEvents::VIEW => ['encodePassword', EventPriorities::PRE_WRITE ]
        ];
    }

    public function encodePassword(ViewEvent $event)
    {

        // Récupère l'utilisateur courant avant l'écriture
        $user = $event->getControllerResult();
        //Récupère la method d'envoir de la requête
        $method = $event->getRequest()->getMethod();

        //Verifie si c'est bien un utilisateur qui récupérer et si la method est bien un POST
        if($user instanceOf User && $method=== Request::METHOD_POST ){
            //Hashage du mot de mot passe claire récupérer et remplacement avant insertion dans la base dce donnée
            $hash = $this->encoder->hashPassword($user, $user->getPassword());
            $user->setPassword($hash);
        }

    }
}
