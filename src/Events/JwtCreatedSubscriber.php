<?php


namespace App\Events;


use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;

class JwtCreatedSubscriber
{

    public function updateJwtData(JWTCreatedEvent $event)
    {
        // 1. Récupérer l'utilisateur courant
        /**
         * @var $currentUser User
         */
        $currentUser = $event->getUser();

        // 2. Enrichire les données de la data
        $data = $event->getData();
        $data['firstName'] = $currentUser->getFirstName();
        $data['lastName'] = $currentUser->getLastName();
        $event->setData($data);

    }
}
