<?php


namespace App\Events;


use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Invoice;
use App\Repository\InvoiceRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Security;

class InvoiceChronoSubscriber implements EventSubscriberInterface
{

    private $security;
    private  $repository;

    public function __construct(Security $security, InvoiceRepository $repository)
    {
        $this->security = $security;
        $this->repository = $repository;
    }


    public static function getSubscribedEvents()
    {
       return [
            KernelEvents::VIEW => ['setChronoForInvoice', EventPriorities::PRE_VALIDATE ]
       ];
    }

    public function setChronoForInvoice(ViewEvent $event)
    {
        $invoice = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();



        if($invoice instanceof Invoice && $method === Request::METHOD_POST){
            // 1. Récupérer l'utilisateur courant
            $currentUser = $this->security->getUser();

            // 2. Récupérer le dernier chrono et créer le chrono suivant pour la factures
            $nextChrono = $this->repository->findLastChrono($currentUser) + 1;

            // 3 .Mettre en place la procédure d'injection
            $invoice->setChrono($nextChrono);

            // TODO : A importer dans une class dédié

            if(empty($invoice->getSentAt())){
                $invoice->setSentAt(new \DateTime());
            }
        }


    }
}
