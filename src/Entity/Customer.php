<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CustomerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Float_;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: CustomerRepository::class)]

/**
 * Class Customer
 * @package App\Entity
 * @ApiResource(
 *  normalizationContext={
 *      "groups" = {"customers_read"}
 *     },
 *  collectionOperations={"GET", "POST"},
 *  itemOperations={"GET", "PUT", "DELETE"},
 *  subresourceOperations={
 *      "invoices_get_subresource"={"path"="/customers/{id}/invoices"}
 *  },
 *  normalizationContext={
 *      "groups"={"customers_read"}
 *  }
 * )
 * @ApiFilter(SearchFilter::class)
 *
 * @ApiFilter(OrderFilter::class)
 */
class Customer
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column()]
    #[Groups(["customers_read", "invoices_read"])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(["customers_read", "invoices_read"])]
    /**
     * @Assert\NotBlank(message="Le prénom du client est obligatoire")
     * @Assert\Length(min="5", max="255", minMessage="Le nombre de caractère ne doit pas être inférieur à 5", maxMessage="Le nombre de caratère ne doit pas être supérieur à 255")
     */
    private ?string $firstName = null;

    #[ORM\Column(length: 255)]
    /**
     * @Groups({"customers_read", "invoices_read"})
     * @Assert\NotBlank(message="Le nom de famille du client est obligatoire")
     * @Assert\Length(min="5", max="255", minMessage="Le nombre de caractère ne doit pas être inférieur à 5", maxMessage="Le nombre de caratère ne doit pas être supérieur à 255")
     */
    private ?string $lastName = null;

    #[ORM\Column(length: 255)]
    #[Groups(["customers_read", "invoices_read"])]
    /**
     * @Assert\NotBlank(message="L'adresse email du client est obligatoire")
     * @Assert\Email(message="Vous devez saisir une adresse email valide")
     */
    private ?string $email = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["customers_read", "invoices_read"])]
    private ?string $company = null ;

    #[ORM\OneToMany(mappedBy: 'customer', targetEntity: Invoice::class)]
    #[Groups(["customers_read"])]
    #[ApiSubresource]
    private Collection $invoices;

    #[ORM\ManyToOne(inversedBy: 'customers')]
    #[Groups(["customers_read"])]
    /**
     * @var User|null
     * @Assert\NotBlank(message="Vous devez préciser un utilisateur valide")
     */
    private ?User $user = null;

    public function __construct()
    {
        $this->invoices = new ArrayCollection();
    }
    /**
     * Permet de récupérer le total des invoices
     * @return float|null
     */
    #[Groups(["customers_read"])]
    public function getTotalAmount(): ?float
    {

        return array_reduce($this->invoices->toArray(), function ($total, $invoice){
            return $total + $invoice->getAmount();
        }, 0);
        
    }

    /**
     * Permet de récupérer le totam des montant des facture impayées ou annulés
     * @return float|null
     */
    #[Groups(["customers_read"])]
    public function getUnpaidAmount(): ?float
    {
        return  array_reduce($this->invoices->toArray(), function ($total, $invoice){
            return $total +($invoice->getStatus() === "PAID" || $invoice->getStatus() === "CANCELLED" ? 0 : $invoice->getAmount());
        }, 0);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(string $company): self
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return Collection<int, Invoice>
     */
    public function getInvoices(): Collection
    {
        return $this->invoices;
    }

    public function addInvoice(Invoice $invoice): self
    {
        if (!$this->invoices->contains($invoice)) {
            $this->invoices[] = $invoice;
            $invoice->setCustomer($this);
        }

        return $this;
    }

    public function removeInvoice(Invoice $invoice): self
    {
        if ($this->invoices->removeElement($invoice)) {
            // set the owning side to null (unless already changed)
            if ($invoice->getCustomer() === $this) {
                $invoice->setCustomer(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
