<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[UniqueEntity('email', message: 'Un utilisateur existe déjà avec cette adresse email')]
/**
 * Class User
 * @package App\Entity
 * @ApiResource(
 *     normalizationContext = {
 *      "groups" = {"users_read"}
 *     },
 *   collectionOperations = { "GET","POST" },
 *   itemOperations = {"GET", "PUT", "DELETE"}
 * )
 */
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column()]
    #[Groups(["users_read", "customers_read", "invoices_read", "invoices_subresource"])]
    private ?int $id = null;

    #[ORM\Column(length: 180, unique: true)]
    #[Groups(["users_read", "customers_read", "invoices_read", "invoices_subresource"])]
    /**
     * @var string|null
     * @Assert\NotBlank(message="L'adresse email est obligatoire")
     * @Assert\Email(message="Saisir un format d'adresse mail valide")
     */
    private ?string $email = null;

    #[ORM\Column]
    private array $roles = [];

    /**
     * @var string The hashed password
     */
    #[ORM\Column]
    /**
     * @var string|null
     * @Assert\NotBlank(message="Le mot de passe est obligatoire")
     */
    private ?string $password = null;

    #[ORM\Column(length: 255)]
    #[Groups(["users_read", "customers_read", "invoices_read", "invoices_subresource"])]
    /**
     * @var string|null
     * @Assert\NotBlank(message="Le prénom est obligatoire")
     * @Assert\Length(min="3", minMessage="Le prénom doit avoir au moins 3 caracatères", max="255", maxMessage="Le prénom doit  avoir au plus 255 caractères")
     */
    private ?string $firstName = null;

    #[ORM\Column(length: 255)]
    #[Groups(["users_read", "customers_read", "invoices_read", "invoices_subresource"])]
    /**
     * @var string|null
     * @Assert\NotBlank(message="Le nom de famille est obligatoire")
     * @Assert\Length(min="3", minMessage="Le nom de famille doit avoir au moins 3 caracatères", max="255", maxMessage="Le nom de famille doit  avoir au plus 255 caractères")
     */
    private ?string $lastName = null;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Customer::class)]
    private Collection $customers;

    /**
     * @var UserPasswordHasherInterface
     */
    private $encoder;

    public function __construct()
    {
        $this->customers = new ArrayCollection();
    }

    #[Groups(["users_read", "customers_read", "invoices_read"])]
    public function getTotal(): ?float
    {
        return array_reduce($this->customers->toArray(), function ($total, $customer){
            return $total + $customer->getTotalAmount();
        }, 0);
    }

    #[Groups(["users_read", "customers_read", "invoices_read"])]
    public function getUnpaidAmount() :?float
    {
        return array_reduce($this->customers->toArray(), function ($total, $customer){
            return $total + $customer->getUnpaidAmount();
        }, 0);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return Collection<int, Customer>
     */
    public function getCustomers(): Collection
    {
        return $this->customers;
    }

    public function addCustomer(Customer $customer): self
    {
        if (!$this->customers->contains($customer)) {
            $this->customers[] = $customer;
            $customer->setUser($this);
        }

        return $this;
    }

    public function removeCustomer(Customer $customer): self
    {
        if ($this->customers->removeElement($customer)) {
            // set the owning side to null (unless already changed)
            if ($customer->getUser() === $this) {
                $customer->setUser(null);
            }
        }

        return $this;
    }
}
