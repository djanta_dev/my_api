<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Repository\InvoiceRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: InvoiceRepository::class)]


/**
 * Class Invoice
 * @package App\Entity
 * @ApiResource(
 *  subresourceOperations={
 *      "api_customers_invoices_get_subresource"={
 *          "normalization_context"={"groups"={"invoices_subresource"}}
 *      }
 *  },
 *  itemOperations={"GET", "PUT", "DELETE"},
 *  attributes={
 *      "pagination_enabled"=false,
 *      "pagination_items_per_page"=20,
 *      "order": {"chrono":"asc"}
 *  },
 *  normalizationContext={"groups"={"invoices_read"}},
 *  denormalizationContext={"disable_type_enforcement"=true}
 * )
 * @ApiFilter(OrderFilter::class)
 * @ApiFilter(SearchFilter::class)
 */
class Invoice
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column()]
    #[Groups(["invoices_read", "customers_read", "invoices_subresource"])]
    private ?int $id = null;

    #[ORM\Column]
    #[Groups(["invoices_read", "customers_read", "invoices_subresource"])]
    /**
     * @var float|null
     * @Assert\NotBlank(message="Le montatnt de la facture est obligatoire.")
     * @Assert\Type(type="numeric", message="Le montant de la facture doit être un numérique")
     */
    private ?float $amount = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups(["invoices_read", "customers_read"])]
    /**
     * @var \DateTimeInterface|null
     * @Assert\NotBlank(message="La date d'envoi de la facture est obligatoire")
     *
     *
     */
    private  $sentAt = null;

    #[ORM\Column(length: 255)]
    #[Groups(["invoices_read", "customers_read", "invoices_subresource"])]
    /**
     * @var string|null
     * @Assert\Choice(choices={"SENT", "CANCELLED", "PAID"}, message="Le statut doit être SENT, PAID ou CANCELLED")
     * @Assert\NotBlank(message="Le statut est obligatoire ")
     */
    private ?string $status = null;

    #[ORM\ManyToOne(inversedBy: 'invoices')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(["invoices_read"])]
    /**
     * @var Customer|null
     * @Assert\NotBlank(message="Le client est obligatoire")
     */
    private ?Customer $customer = null;

    #[ORM\Column]
    #[Groups(["invoices_read", "customers_read", "invoices_subresource"])]
    /**
     * @var int|null
     * @Assert\NotBlank(message="Le chrono est obligatoire")
     * @Assert\Type(type="numeric", message="Le chrono doit être un numérique")
     */
    private $chrono = null;

    public function getId(): ?int
    {
        return $this->id;
    }
    /**
     * Permet de retourner directement les donnés de l'utilisateur
     * @return User|null
     */
    #[Groups(["invoices_read", "invoices_subresource"])]
    public function getUser(): ?User
    {
        return $this->customer->getUser();
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getSentAt(): ?\DateTimeInterface
    {
        return $this->sentAt;
    }

    public function setSentAt(\DateTimeInterface $sentAt): self
    {
        $this->sentAt = $sentAt;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function getChrono(): ?int
    {
        return $this->chrono;
    }

    public function setChrono($chrono): self
    {
        $this->chrono = $chrono;

        return $this;
    }
}
