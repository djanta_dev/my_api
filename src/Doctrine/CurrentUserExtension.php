<?php


namespace App\Doctrine;


use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\Customer;
use App\Entity\Invoice;
use App\Entity\User;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Security;

class CurrentUserExtension implements QueryCollectionExtensionInterface, QueryItemExtensionInterface
{
    private $security;
    private  $auth;


    public function __construct(Security $security, AuthorizationCheckerInterface $checker)
    {
        $this->security = $security;
        $this->auth = $checker;
    }

    private function addWhere(QueryBuilder $queryBuilder, string $resourceClass)
    {
        // 1.  Récupérer l'utilisateur courant
        $currentUser = $this->security->getUser();
        // 2. Récupérer les collections de données concernant l'utilisateur courant
        if( ($resourceClass === Customer::class || $resourceClass === Invoice::class)
            && !$this->auth->isGranted("ROLE_ADMIN")
            && $currentUser instanceof User){
            // 1. récupérer l'alias de la requête
            $rootAlias = $queryBuilder->getRootAliases()[0];

            // 2. On agit suivant que la class soit une Invoice ou un Customer
            if($resourceClass === Customer::class){
                // on ajoute directement l'utilisateur
                $queryBuilder->andWhere("$rootAlias.user = :user");
            }elseif ($resourceClass === Invoice::class){
                // on fait un join pour aller chercher l'utilisateur dans le customer
                $queryBuilder->join("$rootAlias.customer", "c")
                    ->andWhere("c.user = :user");
            }

            $queryBuilder->setParameter("user", $currentUser);

        }
    }

    public function applyToCollection(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null)
    {
        $this->addWhere($queryBuilder, $resourceClass);
    }

    public function applyToItem(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, array $identifiers, string $operationName = null, array $context = [])
    {
        $this->addWhere($queryBuilder, $resourceClass);
    }
}
